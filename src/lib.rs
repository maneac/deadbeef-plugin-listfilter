use std::ffi::CString;

mod imports {
    #![allow(non_upper_case_globals, non_camel_case_types, non_snake_case)]
    include!(concat!(env!("OUT_DIR"), "/deadbeef.rs"));
}

const NO: i8 = 1;

static mut LIST_FILTER: imports::DB_plugin_t = imports::DB_plugin_t {
    type_: 0,
    version_major: 0,
    version_minor: 1,

    id: &NO,
    name: &NO,
    descr: &NO,
    copyright: &NO,
    website: &NO,

    configdialog: &NO,

    api_vmajor: 0,
    api_vminor: 0,
    flags: 0,
    reserved1: 0,
    reserved2: 0,
    reserved3: 0,

    command: None,
    connect: None,
    disconnect: None,
    exec_cmdline: None,
    get_actions: None,
    message: None,
    start: None,
    stop: None,
};

#[no_mangle]
unsafe extern "C" fn listfilter_load(
    _api: *const imports::DB_functions_t,
) -> *const imports::DB_plugin_t {
    let dialog = CString::new(
        r#"property "Enable" checkbox ddb_listfilter.checked 0;
"#,
    )
    .unwrap();

    let name = CString::new("Playlist Filters").unwrap();
    let id = CString::new("listfilter").unwrap();

    let description =
        CString::new("Enables the creation of a playlist from another using filters").unwrap();

    let website = CString::new("http://gitlab.com/maneac/deadbeef-plugin-listfilter").unwrap();
    let copyright = CString::new(include_str!("../LICENSE")).unwrap();

    LIST_FILTER = imports::DB_plugin_t {
        type_: imports::DB_PLUGIN_MISC as i32,
        version_major: 0,
        version_minor: 1,

        id: id.into_raw(),
        name: name.into_raw(),
        descr: description.into_raw(),
        copyright: copyright.into_raw(),
        website: website.into_raw(),

        configdialog: dialog.into_raw(),

        api_vmajor: 1,
        api_vminor: 5,
        flags: 0,
        reserved1: 0,
        reserved2: 0,
        reserved3: 0,

        command: None,
        connect: None,
        disconnect: None,
        exec_cmdline: None,
        get_actions: None,
        message: None,
        start: None,
        stop: None,
    };

    &LIST_FILTER
}
